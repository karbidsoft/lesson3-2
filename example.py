import requests
import os.path

API_KEY = 'trnsl.1.1.20170402T142136Z.5075abd08cc8f582.42595d661ece26db7cc1ae2304f099c0d571486a'
URL = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

def translate_it(text, from_lang, to_lang):
    params = {
        'key': API_KEY,
        'text': text,
        'lang': '{}-{}'.format(from_lang, to_lang),
    }

    response = requests.get(URL, params=params)
    json_ = response.json()
    return ''.join(json_['text'])


def fetch_source_file(file):
    path = os.path.join(BASE_DIR, file)
    with open(path, 'r', encoding='utf-8') as file:
        source_text = file.read()
    return source_text

def save_dest_file(file, data):
    path = os.path.join(BASE_DIR, file)
    with open(path, 'w', encoding='utf-8') as file:
        file.write(data)

def user_settings():
    from_lang = input('Введите язык с которого переводим: ')
    to_lang = input('Введите язык на который переводим (пустая строка ru): ')
    if len(to_lang) < 1:
        to_lang = 'ru'
    source_file =  input('Укажите имя файла источника: ')
    dest_file = input('Укажите имя файла назначения: ')
    text = fetch_source_file(source_file)
    result = translate_it(text, from_lang, to_lang)
    if len(result) > 0:
        save_dest_file(dest_file, result)

user_settings()
